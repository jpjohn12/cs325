package assignments.visitor;

public interface Room {
	
	public String accept(Visitor visitor);

}
