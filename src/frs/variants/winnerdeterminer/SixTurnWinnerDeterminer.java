package frs.variants.winnerdeterminer;

import frs.core.GameImpl;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Color;
import frs.hotgammon.Game;

public class SixTurnWinnerDeterminer implements WinnerDeterminer {
	private Game game;
	
	public SixTurnWinnerDeterminer(Game game) {
		this.game = game;
	}

	@Override
	public Color winner() {
		if (((GameImpl) game).getThrowCount() < 6) {
			  return Color.NONE; 
		  }
		  else {
			  return Color.RED;
		  }
		
	}

	
}
