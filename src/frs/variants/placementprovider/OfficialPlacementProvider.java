package frs.variants.placementprovider;

import frs.core.Board.Placement;
import frs.core.GameImpl.GamePoint;
import frs.core.PlacementProvider;
import frs.hotgammon.Color;
import frs.hotgammon.Location;

public class OfficialPlacementProvider implements PlacementProvider {
	private Placement[] placementArray;

	@Override
	public Placement[] placementArray() {
		placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)),
										  new Placement(Location.R6, new GamePoint(Color.RED, 5)),
										  new Placement(Location.R8, new GamePoint(Color.RED, 3)),
										  new Placement(Location.R12, new GamePoint(Color.BLACK, 5)),
										  new Placement(Location.B12, new GamePoint(Color.RED, 5)),
										  new Placement(Location.B8, new GamePoint(Color.BLACK, 3)),
										  new Placement(Location.B6, new GamePoint(Color.BLACK, 5)),
										  new Placement(Location.B1, new GamePoint(Color.RED, 2))};
		
		return placementArray;
		
	}
	

}
