package assignments.farmer;

import java.util.HashMap;
import java.util.Map;

public class AmishFarmer implements Farmer {
	Operations springOps;
	Operations summerOps;
	Operations fallOps;
	Operations currentOps;
	Map<String, Operations> map = new HashMap<String, Operations>();
	
	public AmishFarmer(Operations springOps, Operations summerOps, Operations fallOps) {
		this.springOps = springOps;
		this.summerOps = summerOps;
		this.fallOps = fallOps;
		map.put("spring", springOps);
		map.put("summer", summerOps);
		map.put("fall", fallOps);
	}

	@Override
	public void plow() {
		System.out.println(currentOps.getPlow());
		
	}

	@Override
	public void plant() {
		System.out.println(currentOps.getPlant());
		
	}

	@Override
	public void weedControl() {
		System.out.println(currentOps.getWeedControl());
		
	}

	@Override
	public void harvest() {
		System.out.println(currentOps.getHarvest());
		
	}

	@Override
	public void market() {
		System.out.println(currentOps.getMarket());
		
	}
	
	public void setSeason(String season) {
		this.currentOps = map.get(season);
	}

}
