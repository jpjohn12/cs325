package assignments.transformation;

public interface Command {
	public void setNext(Command next);
	
	public String[] execute(String command, String arg, String... s);
	
	public Command inverse();

}
