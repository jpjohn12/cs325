package assignments.visitor;

public class Kitchen implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.prepareKitchen(this);
		
	}

}
