package assignments.visitor;

public class Den implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.prepareDen(this);
		
	}

}
