package assignments.farmer;

public class ConventionalSummer {
	Operations operations;
	
	public ConventionalSummer() {
		operations = new Operations("Conventional Plow: using no-till; no plowing", "Conventional Plant: corn", "Conventional Weed Control: spray",
				"Conventional Harvest: nothing to harvest", "Conventional Market: nothing to market");
	}
	
	public Operations getOperations() {
		return operations;
	}

}
