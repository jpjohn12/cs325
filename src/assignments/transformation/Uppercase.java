package assignments.transformation;

public class Uppercase implements Command {
	private Command chain;
	
	@Override
	public void setNext(Command next) {
		this.chain = next;
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("uppercase")) {
			for (int i = 0; i < s.length; i++) {
				s[i] = s[i].toUpperCase();
			}
			
			return s;
		}
		else {
			return this.chain.execute(command, arg, s);
		}
	}

	@Override
	public Command inverse() {
		return new Lowercase();
	}
	
	public String toString() {
		return "uppercase";
	}

}
