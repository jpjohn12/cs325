package assignments.farmer;

public class OrganicSummer {
	Operations operations;
	
	public OrganicSummer() {
		operations = new Operations("Organic Plow: plow fallow fields to prepare for fall cover crop", "Organic Plant: beans, squash, tomatoes, carrots, melons, 2nd round of peas and leafy greens",
				"Organic Weed Control: employ lots of interns with hoes", "Organic Harvest: employ lots of interns and volunteers",
				"Organic Market: peas, carrots, early beans, roma tomatoes to farmer's market");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
