package frs.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import frs.alphamon.AlphamonTest;
import frs.backgammon.BackgammonTest;
import frs.betamon.BetamonTest;
import frs.deltamon.DeltamonTest;
import frs.epsilonmon.EpsilonmonTest;
import frs.gammamon.GammamonTest;
import frs.hotgammon.LocationTest;
import frs.semimon.SemimonTest;
import frs.zetamon.ZetamonTest;


@RunWith(Suite.class)
@Suite.SuiteClasses({LocationTest.class, AlphamonTest.class, BetamonTest.class, GammamonTest.class, DeltamonTest.class,
	EpsilonmonTest.class, ZetamonTest.class, SemimonTest.class, BackgammonTest.class})

public class allTests {
	
	

}
