package assignments.farmer;

public class Main {
	public static void main(String[] args) {
		Farmer amish = new AmishFarmer(new AmishSpring().getOperations(), new AmishSummer().getOperations(), new AmishFall().getOperation());
		Farmer conventional = new ConventionalFarmer(new ConventionalSpring().getOperations(), new ConventionalSummer().getOperations(), new ConventionalFall().getOperations());
		Farmer organic = new OrganicFarmer(new OrganicSpring().getOperations(), new OrganicSummer().getOperations(), new OrganicFall().getOperations());
		Farmer[] farmers = new Farmer[] {amish, conventional, organic};
		String[] seasons = new String[] {"spring", "summer", "fall"};
		for (int i = 0; i < seasons.length; i++) {
			amish.setSeason(seasons[i]);
			conventional.setSeason(seasons[i]);
			organic.setSeason(seasons[i]);
			System.out.println("\n" + seasons[i] + ":\n");
			for (int j = 0; j < farmers.length; j++) {
				farmers[j].plow();
				farmers[j].plant();
				farmers[j].weedControl();
				farmers[j].harvest();
				farmers[j].market();
			}
		}
	}

}
