package assignments.farmer;

public class AmishSummer {
	Operations operations;
	
	public AmishSummer() {
		operations = new Operations("Amish Plow: no action", "Amish Plant: beans, squash, beets, tomatoes, carrots", "Amish Weed Control: walking cultivator in garden, two-row, horse-drawn cultivator in fields",
				"Amish Harvest: wife and kids help out in garden; neighbors help with oats and hay", "Amish Market: peas, carrots, early beans, roma tomatoes to auction");
		
	}
	
	
	public Operations getOperations() {
		return operations;
	}

}
