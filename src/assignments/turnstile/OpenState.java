package assignments.turnstile;

public class OpenState implements State {
	public static final String INSERT = "You already inserted a token.";
	public static final String ENTER = "Please Enter.";
	public static final String LEAVE = "Have a nice trip.";
	
	private Turnstile turnstile;
	
	public OpenState(Turnstile turnstile) {
		this.turnstile = turnstile;
	}
	
	@Override
	public String insertToken() {
		return INSERT;
	}

	@Override
	public String enter() {
		return ENTER;
	}

	@Override
	public String leave() {
		turnstile.setState(new ClosedState(turnstile));
		return LEAVE;
	}

}
