package assignments.transformation;

public class Undo implements Command {
	Transformer transformer;
	Command chain;
	
	public Undo(Transformer transformer) {
		this.transformer = transformer;
	}

	@Override
	public void setNext(Command next) {
		this.chain = next;
		
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("undo")) {
			transformer.undo();
			return transformer.getContext();
		}
		else {
			return this.chain.execute(command, arg, s);
		}
	}

	@Override
	public Command inverse() {
		return null;
	}
	
	public String toString() {
		return "undo";
	}

}
