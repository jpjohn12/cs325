package frs.hotgammon.view;

import java.awt.Point;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.standard.StandardDrawing;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.GameObserver;
import frs.hotgammon.Location;
import frs.hotgammon.view.figures.DieFigure;

public class HotgammonDrawing extends StandardDrawing implements GameObserver {
	private DrawingEditor editor;
	private Game game;
	
	private static final Point DIE_POINT_1 = new Point(216, 202);
	private static final Point DIE_POINT_2 = new Point(306, 202);
	private static final Point DIE_POINT_3 = new Point(160, 202);
	private static final Point DIE_POINT_4 = new Point(362, 202);
	
	public HotgammonDrawing(DrawingEditor editor, Game game) {
		this.editor = editor;
		this.game = game;
	}

	@Override
	public void checkerMove(Location from, Location to) {
		Point fromPoint = Convert.locationAndCount2xy(from, game.getCount(from) - 1);
		Point toPoint = Convert.locationAndCount2xy(to, game.getCount(to) - 1);
		Point barPoint = Convert.locationAndCount2xy(to, game.getCount(to));
		
		if ((to == Location.R_BAR || to == Location.B_BAR) && editor.drawing().selection().size() > 1) {
			Figure figure = editor.drawing().selection().get(1);
			figure.moveBy(barPoint.x - fromPoint.x, barPoint.y - fromPoint.y);
		}
		else {
			Figure figure = editor.drawing().selection().get(0);
			figure.moveBy(toPoint.x - figure.displayBox().x, toPoint.y - figure.displayBox().y);	
		}
		if (game.winner() != Color.NONE) {
			editor.showStatus(game.winner() + " Wins!");
		}
	
		
		
}

	@Override
	public void diceRolled(int[] values) {
		if (editor.drawing().findFigure(362, 202) != null && editor.drawing().findFigure(160, 202) != null) {
			editor.drawing().remove(editor.drawing().findFigure(DIE_POINT_3.x, DIE_POINT_3.y));
			editor.drawing().remove(editor.drawing().findFigure(DIE_POINT_4.x, DIE_POINT_4.y));
		}
		editor.drawing().add(new DieFigure(values[0], DIE_POINT_1));
		editor.drawing().add(new DieFigure(values[1], DIE_POINT_2));
		if (game.diceThrown()[0] == game.diceThrown()[1]) {
			editor.drawing().add(new DieFigure(values[0], DIE_POINT_3));
			editor.drawing().add(new DieFigure(values[1], DIE_POINT_4));
		}		
	}
	
	

}
