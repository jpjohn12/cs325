package frs.variants.dieroller;

import frs.core.DieRoller;

public class IncrementalDieRoller implements DieRoller {
	private int[][] diceThrows = { {1, 2}, {3, 4}, {5, 6} };

	@Override
	public int[] rollDie(int throwCount) {
		return diceThrows[throwCount % diceThrows.length];
		
	}
	
}
