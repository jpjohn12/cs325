package frs.betamon;

import javax.swing.JTextField;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.DrawingView;
import frs.core.Board.Placement;
import frs.core.DieRoller;
import frs.core.HotGammonFactory;
import frs.core.MoveValidator;
import frs.core.PlacementProvider;
import frs.core.StartingPlayerDeterminer;
import frs.core.TurnChanger;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Game;
import frs.variants.dieroller.IncrementalDieRoller;
import frs.variants.movevalidator.OfficialMoveValidator;
import frs.variants.startingplayerdeterminer.BlackStartingPlayerDeterminer;
import frs.variants.turnchanger.AlternatingTurnChanger;
import frs.variants.winnerdeterminer.SixTurnWinnerDeterminer;

public class BetamonFactory implements HotGammonFactory {
	private Placement[] placementArray;
	private Game game;

	@Override
	public MoveValidator createMoveValidator() {
		return new OfficialMoveValidator(game);
	}

	@Override
	public WinnerDeterminer createWinnerDeterminer() {
		return new SixTurnWinnerDeterminer(game);
	}

	@Override
	public TurnChanger createTurnChanger() {
		return new AlternatingTurnChanger(game);
	}

	@Override
	public DieRoller createDieRollCalculator() {
		return new IncrementalDieRoller();
	}

	@Override
	public StartingPlayerDeterminer createStartingPlayerDeterminer() {
		return new BlackStartingPlayerDeterminer();
	}

	@Override
	public void setGameImpl(Game game) {
		this.game = game;
		
	}
	
	@Override
	public PlacementProvider createPlacementProvider() {
		return new PlacementProvider() {
			public Placement[] placementArray() {
				return placementArray;
			}
			
		};
	}
	
	public void setPlacement(Placement[] placementArray) {
		this.placementArray = placementArray;
	}

	@Override
	public DrawingView createDrawingView(DrawingEditor editor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Drawing createDrawing(DrawingEditor editor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JTextField createStatusField(DrawingEditor editor) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
