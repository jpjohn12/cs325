package assignments.farmer;

public class AmishSpring {
	Operations operations;
	
	public AmishSpring() {
		operations = new Operations("Amish Plow: plow corn fields", "Amish Plant: peas, lettuce, oats", "Amish Weed Control: walking, cultivator in garden",
				"Amish Harvest: wife and kids help out", "Amish Market: jams, jellies, peas and lettuce to auction");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
