package assignments.farmer;

public class ConventionalSpring {
	Operations operations;
	
	public ConventionalSpring() {
		operations = new Operations("Conventional Plow: using no-till; no plowing", "Conventional Plant: no action", "Conventional Weed Control: no action", 
				"Conventional Harvest: nothing to harvest", "Conventional Market: nothing to market");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
