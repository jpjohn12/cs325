package frs.backgammon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import frs.core.GameImpl;
import frs.core.HotGammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.TestObserver;

public class BackgammonTest {
	private HotGammonFactory factory;
	private Game game;

	@Before
	public void setup() {
		factory = new BackgammonFactory();
		game = new GameImpl(factory);
		game.addObserver(new TestObserver());
	}
	
	@Test
	public void blackShouldStartWith2CheckersOnR1() {
		game.newGame();
		assertEquals(2, game.getCount(Location.R1));
		assertEquals(Color.BLACK, game.getColor(Location.R1));
	}
	
	@Test
	public void redShouldStartWith2CheckersOnB1() {
		game.newGame();
		assertEquals(2, game.getCount(Location.B1));
		assertEquals(Color.RED, game.getColor(Location.B1));
	}
	
	@Test
	public void blackShouldStartWith5CheckersOnB6() {
		game.newGame();
		assertEquals(5, game.getCount(Location.B6));
		assertEquals(Color.BLACK, game.getColor(Location.B6));
	}
	
	@Test
	public void redShouldStartWith5CheckersOnR6() {
		game.newGame();
		assertEquals(5, game.getCount(Location.R6));
		assertEquals(Color.RED, game.getColor(Location.R6));
	}
	
	@Test
	public void blackShouldStartWith3CheckersOnB8() {
		game.newGame();
		assertEquals(3, game.getCount(Location.B8));
		assertEquals(Color.BLACK, game.getColor(Location.B8));
	}
	
	@Test
	public void redShouldStartWith3CheckersOnR8() {
		game.newGame();
		assertEquals(3, game.getCount(Location.R8));
		assertEquals(Color.RED, game.getColor(Location.R8));
	}
	
	@Test
	public void blackShouldStartWith5CheckersOnR12() {
		game.newGame();
		assertEquals(5, game.getCount(Location.R12));
		assertEquals(Color.BLACK, game.getColor(Location.R12));
	}
	
	@Test
	public void redShouldStartWith5CheckersOnB12() {
		game.newGame();
		assertEquals(5, game.getCount(Location.B12));
		assertEquals(Color.RED, game.getColor(Location.B12));
	}
  
	@Test
	public void cannotMoveWhenNotInTurn() {
		game.newGame();
		game.nextTurn();
		assertFalse(game.move(Location.B1, Location.B2));
	}

}
