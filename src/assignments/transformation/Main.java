package assignments.transformation;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
	private static Transformer transformer;
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		HashMap<String, Command> map = new HashMap<String, Command>() {{
			put("split", new Split());
			put("join", new Join());
			put("uppercase", new Uppercase());
			put("lowercase", new Lowercase());
			put("reverse", new Reverse());
		}};
		transformer = new Transformer(map);
		Command c = createChain();
		System.out.print("Enter a string: ");
		String context = input.nextLine();
		transformer.setContext(new String[] {context});
		System.out.println(context.toString());
		String s = null;
		while(s != "quit") {
			System.out.print("Transformation? ");
			String[] strings = input.nextLine().split(" ");
			String cmd = strings[0];
			String arg = null;
			if (strings.length > 1) {
				arg = strings[1];
			}
			if (cmd.equals("quit")) {
				s = "quit";
				break;
			}
			if (cmd.equals("undo")) {
				context = transformer.undo();
				System.out.println(context.toString());
			}
			else {
				transformer.setCommand(c);
				context = transformer.execute(cmd, arg).toString();
				System.out.println(context.toString());
			}
		
			
		}
		
	}
	
	private static Command createChain() {
		Command c1 = new Uppercase();
		Command c2 = new Lowercase();
		c1.setNext(c2);
		Command c3 = new Reverse();
		c2.setNext(c3);
		Command c4 = new Split();
		c3.setNext(c4);
		Command c5 = new Join();
		c4.setNext(c5);
		c5.setNext(new Invalid());
		return c1;
	}

}
