package frs.variants.winnerdeterminer;

import frs.core.GameImpl;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class OfficialWinnerDeterminer implements WinnerDeterminer {
	private Game game;
	
	public OfficialWinnerDeterminer(Game game) {
		this.game = game;
	}

	@Override
	public Color winner() {
		if (((GameImpl) game).getCheckerCount() == 0) {
			return Color.NONE;
		}
		
		if (game.getPlayerInTurn() == Color.BLACK) {
			if (game.getCount(Location.B_BEAR_OFF) == ((GameImpl) game).getBlackCheckerCount()) {
				return Color.BLACK;
			}
		}
		else if (game.getPlayerInTurn() == Color.RED) {
			if (game.getCount(Location.R_BEAR_OFF) == ((GameImpl) game).getRedCheckerCount()) {
				return Color.RED;
			}
		}
		return Color.NONE;
	}

	
}
