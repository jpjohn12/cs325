package assignments.transformation;

public class Invalid implements Command {
	
	public String[] execute(String command, String arg, String... s) {
		System.out.println("Input is invalid please try again.");
		return new String[] {"-1"};
		
	}

	@Override
	public void setNext(Command next) {
		
		
	}

	@Override
	public Command inverse() {
		// TODO Auto-generated method stub
		return this;
	}
	
	public String toString() {
		return "invalid";
	}

}
