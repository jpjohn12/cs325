package frs.main;




import java.awt.Point;

import javax.swing.JTextField;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.DrawingView;
import minidraw.framework.Factory;
import minidraw.framework.Figure;
import minidraw.standard.MiniDrawApplication;
import minidraw.standard.StdViewWithBackground;
import frs.backgammon.BackgammonFactory;
import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.HotGammonFactory;
import frs.core.PlacementProvider;
import frs.hotgammon.Game;
import frs.hotgammon.GameObserver;
import frs.hotgammon.view.Convert;
import frs.hotgammon.view.HotgammonDrawing;
import frs.hotgammon.view.figures.CheckerFigure;
import frs.hotgammon.view.figures.DieFigure;
import frs.hotgammon.view.tools.DieRollTool;

public class Hotgammon {
	public static void main(String[] args) {
		HotGammonFactory factory = new BackgammonFactory();
//		Placement[] placements = new Placement[] {new Placement(Location.R2, new GamePoint(Color.RED, 2)), new Placement(Location.R3, new GamePoint(Color.RED, 2)), new Placement(Location.R1, new GamePoint(Color.BLACK, 1))};
//		factory.setPlacement(placements);
		Game game = new GameImpl(factory);
		DrawingEditor editor = new MiniDrawApplication("Hotgammon", new HotgammonFactory(game));
		editor.open();
		GameObserver drawing = new HotgammonDrawing(editor, game);
		game.addObserver(drawing);
		game.newGame();
		
		
		Figure redDie = new DieFigure(1, new Point(216, 202));
		Figure blackDie = new DieFigure(2, new Point(306, 202));
		editor.drawing().lock();
		editor.drawing().add(redDie);
		editor.drawing().add(blackDie);
		
		PlacementProvider placements = factory.createPlacementProvider();
		for (Placement p : placements.placementArray()) {
			for (int i = 0; i < p.point.getCount(); i++) {
				editor.drawing().add(new CheckerFigure(p.point.getPlayer(), Convert.locationAndCount2xy(p.location, i)));
			}
		}
		editor.drawing().unlock();
		editor.setTool(new DieRollTool(editor, game));
		
	}

}

class HotgammonFactory implements Factory {
	Game game;
	public HotgammonFactory(Game game) {
		this.game = game;
	}
	
	@Override
	public DrawingView createDrawingView(DrawingEditor editor) {
		DrawingView view = new StdViewWithBackground(editor, "board");
		return view;
	}

	@Override
	public Drawing createDrawing(DrawingEditor editor) {
		return new HotgammonDrawing(editor, game);
	}

	@Override
	public JTextField createStatusField(DrawingEditor editor) {
		JTextField textField = new JTextField( "Click The Dice To Start The Game" );
		textField.setEditable(false);
		return textField;
	}
}



