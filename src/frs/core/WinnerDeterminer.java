package frs.core;

import frs.hotgammon.Color;

public interface WinnerDeterminer {
	
	public Color winner();

	
}
