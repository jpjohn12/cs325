package assignments.farmer;

public class AmishFall {
	Operations operations;
	
	public AmishFall() {
		operations = new Operations("Amish Plow: no action", "Amish Plant: late beans, squash, tomatoes", "Amish Weed Control: no action", "Amish Harvest: wife and kids help out in garden, neighbors help with corn and hay",
				"Amish Market: beans, squash, tomatoes to auction");

	}
	
	public Operations getOperation() {
		return operations;
	}

}
