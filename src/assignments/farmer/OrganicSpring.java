package assignments.farmer;

public class OrganicSpring {
	Operations operations;
	
	public OrganicSpring() {
		operations = new Operations("Organic Plow: plow under green manure", "Organic Plant: peas, lettuce", "Organic Weed Control: employ lots of interns with hoes",
				"Organic Harvest: employ lots of interns and volunteers", "Organic Market: fall garlic, peas and lettuce to farmer's market");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
