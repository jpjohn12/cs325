package assignments.turnstile;

public class ClosedState implements State {
	public static final String INSERT = "Thank you. Please enter.";
	public static final String ENTER = "Alarm goes off.";
	public static final String LEAVE = "You haven't paid or entered yet.";
	
	private Turnstile turnstile;
	
	public ClosedState(Turnstile turnstile) {
		this.turnstile = turnstile;
	}

	@Override
	public String insertToken() {
		turnstile.setState(new OpenState(turnstile));
		return INSERT;
	}
	
	@Override
	public String enter() {
		return ENTER;
	}

	@Override
	public String leave() {
		return LEAVE;
	}

}
