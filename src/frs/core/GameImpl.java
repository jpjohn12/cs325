package frs.core;

/** Skeleton implementation of HotGammon.

 This source code is from the book 
 "Flexible, Reliable Software:
 Using Patterns and Agile Development"
 published 2010 by CRC Press.
 Author: 
 Henrik B Christensen 
 Department of Computer Science
 Aarhus University

 Please visit http://www.baerbak.com/ for further information.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */

import java.util.ArrayList;

import frs.core.Board.Placement;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.GameObserver;
import frs.hotgammon.Location;

public class GameImpl implements Game {
	private Color playerInTurn = Color.NONE;
	private int throwCount = -1;
	private int[] diceThrown;
	public int[] diceValuesLeft;

	private MoveValidator moveValidator;
	private WinnerDeterminer winnerDeterminer;
	private TurnChanger turnChanger;
	private DieRoller dieRollCalculator;
	private StartingPlayerDeterminer startingPlayerDeterminer;
	private PlacementProvider placementProvider;
	private GameObserver observer;

	public static class GamePoint {
		Color player = Color.NONE;
		int count;

		public GamePoint() {

		}

		public GamePoint(Color color, int count) {
			this.player = color;
			this.count = count;
		}

		public Color getPlayer() {
			return player;
		}

		public int getCount() {
			return count;
		}

		public void setPlayer(Color player) {
			this.player = player;
		}

		public void setCount(int count) {
			this.count = count;
		}
	}

	public GameImpl(HotGammonFactory factory) {
		factory.setGameImpl(this);
		this.moveValidator = factory.createMoveValidator();
		this.winnerDeterminer = factory.createWinnerDeterminer();
		this.turnChanger = factory.createTurnChanger();
		this.dieRollCalculator = factory.createDieRollCalculator();
		this.startingPlayerDeterminer = factory
				.createStartingPlayerDeterminer();
		this.placementProvider = factory.createPlacementProvider();

	}

	public Board board = new Board();

	public void initBoard() {
		for (Location l : Location.values()) {
			board.addPlacement(new Placement(l, new GamePoint()));
		}

	}

	public void newGame() {
		initBoard();
		if (placementProvider.placementArray() != null) {
			configureBoard(placementProvider.placementArray());
		}
	}

	public void configureBoard(Placement[] placementArray) {
		for (Placement p : placementArray) {
			board.addPlacement(p);
		}
	}

	public void nextTurn() {
		if (winner() == Color.NONE) {
			throwCount++;

			if (throwCount == 0) {
				diceThrown = dieRollCalculator.rollDie(throwCount);
				while (diceThrown[0] == diceThrown[1]) {
					diceThrown = dieRollCalculator.rollDie(throwCount);
				}
				diceValuesLeft = diceThrown;
				playerInTurn = startingPlayerDeterminer
						.determineStartingPlayer(diceThrown[0], diceThrown[1]);
			} else if (throwCount > 0) {
				playerInTurn = turnChanger.changeTurns();
				diceThrown = dieRollCalculator.rollDie(throwCount);
				diceValuesLeft = diceThrown;
			}
			observer.diceRolled(diceThrown());
		}
	}

	public boolean move(Location from, Location to) {
		if (from == null || to == null) {
			return false;
		}
		if (getColor(from) != playerInTurn) {
			return false;
		}
		
		if (moveValidator.isValid(from, to)) {
			if (isCapturing(to)) {
				if (getPlayerInTurn() == Color.BLACK) {
					observer.checkerMove(to, Location.R_BAR);
				} else if (getPlayerInTurn() == Color.RED) {
					observer.checkerMove(to, Location.B_BAR);
				}
				board.get(to).setCount(board.get(to).getCount() - 1);

				if (playerInTurn == Color.BLACK) {
					board.addPlacement(new Placement(Location.R_BAR,
							new GamePoint(Color.RED, board.get(Location.R_BAR)
									.getCount() + 1)));
				} else {
					board.addPlacement(new Placement(Location.B_BAR,
							new GamePoint(Color.BLACK, board
									.get(Location.B_BAR).getCount() + 1)));
				}
				board.capture(playerInTurn);
			} else {
//				board.get(to).setCount(board.get(to).getCount() + 1);
			}
			if (to == Location.R_BAR || to == Location.B_BAR) {
				observer.checkerMove(to, from);
				return false;
			}
			if (board.get(from).getPlayer() != playerInTurn) {
				observer.checkerMove(to, from);
				return false;
			}
			board.get(to).setPlayer(playerInTurn);
			board.get(from).setCount(board.get(from).getCount() - 1);
			if (board.get(from).getCount() == 0) {
				board.get(from).setPlayer(Color.NONE);
			}
			board.get(to).setCount(board.get(to).getCount() + 1);
			removeDieFromValuesLeft(Math.abs(Location.distance(from, to)));
			if (to == Location.B_BEAR_OFF || to == Location.R_BEAR_OFF) {
				boolean moved = false;
				for (int die : diceValuesLeft) {
					if (Location.distance(from, to) < die) {
						observer.checkerMove(
								Location.findLocation(playerInTurn, to,
										-Location.distance(from, to)), to);
						moved = true;
					}
				}
				if (moved == false) {
					observer.checkerMove(from, to);
				}
			} else {
				observer.checkerMove(from, to);
			}
			return true;
		} else {
			observer.checkerMove(to, from);
			return false;
		}
	}
	
	public boolean isCapturing(Location to) {
		return playerInTurn != getColor(to) && getCount(to) == 1;
	}

	private void removeDieFromValuesLeft(int distance) {
		ArrayList<Integer> array = new ArrayList<Integer>();
		boolean distanceEqualsDie = false;
		int dieToRemove = 0;
		for (int die : diceValuesLeft) {
			array.add(die);
			if (die == distance) {
				distanceEqualsDie = true;
				dieToRemove = die;
			}

		}
		if (distanceEqualsDie == false) {

			for (int die : diceValuesLeft) {
				if (die > distance) {
					dieToRemove = die;
					break;
				}

			}

		}
		for (int i = 0; i < diceValuesLeft.length; i++) {
			if (diceValuesLeft[i] == dieToRemove) {
				array.remove(i);
				break;
			}
		}
		int[] newDiceValues = new int[diceValuesLeft.length - 1];
		if (array.size() > 0) {
			for (int i = 0; i < newDiceValues.length; i++) {
				newDiceValues[i] = array.get(i);
			}
		}
		diceValuesLeft = newDiceValues;

	}

	public Color getPlayerInTurn() {
		return playerInTurn;
	}

	public int getNumberOfMovesLeft() {
		return diceValuesLeft.length;
	}

	public int[] diceThrown() {

		return diceThrown;
	}

	public int[] diceValuesLeft() {
		int[] array = new int[diceValuesLeft.length];
		for (int i = 0; i < array.length; i++) {
			array[i] = diceValuesLeft[i];
		}
		if (array.length > 1) {
			if (array[0] < array[1]) {
				int temp = array[0];
				array[0] = array[1];
				array[1] = temp;
			}
		}

		return array;
	}

	public Color winner() {
		return winnerDeterminer.winner();
	}

	public Color getColor(Location location) {
		return board.get(location).player;
	}

	public int getCount(Location location) {
		return board.get(location).count;
	}

	@Override
	public void addObserver(GameObserver gl) {
		this.observer = gl;

	}

	public int getCheckerCount() {
		return board.getCheckerCount();
	}

	public int getBlackCheckerCount() {
		return board.getBlackCheckerCount();
	}

	public int getRedCheckerCount() {
		return board.getRedCheckerCount();
	}
	
	public int getThrowCount() {
		return throwCount;
	}
}