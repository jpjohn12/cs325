package frs.backgammon;

import javax.swing.JTextField;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.DrawingView;
import minidraw.framework.Factory;
import minidraw.standard.StdViewWithBackground;
import frs.core.Board.Placement;
import frs.core.DieRoller;
import frs.core.HotGammonFactory;
import frs.core.MoveValidator;
import frs.core.PlacementProvider;
import frs.core.StartingPlayerDeterminer;
import frs.core.TurnChanger;
import frs.core.WinnerDeterminer;
import frs.hotgammon.Game;
import frs.hotgammon.view.HotgammonDrawing;
import frs.variants.dieroller.RandomDieRoller;
import frs.variants.movevalidator.OfficialMoveValidator;
import frs.variants.placementprovider.OfficialPlacementProvider;
import frs.variants.startingplayerdeterminer.DiceRollStartingPlayerDeterminer;
import frs.variants.turnchanger.AlternatingTurnChanger;
import frs.variants.winnerdeterminer.OfficialWinnerDeterminer;

public class BackgammonFactory implements HotGammonFactory, Factory {
	private Game game;
	
	private JTextField textField;

	@Override
	public MoveValidator createMoveValidator() {
		return new OfficialMoveValidator(game);
	}

	@Override
	public WinnerDeterminer createWinnerDeterminer() {
		return new OfficialWinnerDeterminer(game);
	}

	@Override
	public TurnChanger createTurnChanger() {
		return new AlternatingTurnChanger(game);
	}

	@Override
	public DieRoller createDieRollCalculator() {
		return new RandomDieRoller();
	}

	@Override
	public StartingPlayerDeterminer createStartingPlayerDeterminer() {
		return new DiceRollStartingPlayerDeterminer();
	}

	@Override
	public void setGameImpl(Game game) {
		this.game = game;	
	}

	@Override
	public PlacementProvider createPlacementProvider() {
		return new OfficialPlacementProvider();
	}
	
	public void setPlacement(Placement[] placementArray) {
		
	}

	@Override
	public DrawingView createDrawingView(DrawingEditor editor) {
		DrawingView view = new StdViewWithBackground(editor, "board");
		return view;
	}

	@Override
	public Drawing createDrawing(DrawingEditor editor) {
		return new HotgammonDrawing(editor, game);
	}

	@Override
	public JTextField createStatusField(DrawingEditor editor) {
		textField = new JTextField( "Click The Dice To Start The Game" );
	    textField.setEditable(false);
	    return textField;
	}
	
	

}
