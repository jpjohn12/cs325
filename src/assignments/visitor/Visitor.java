package assignments.visitor;

public interface Visitor {
	
	public String prepareBathroom(Bathroom bathroom);
	
	public String prepareKitchen(Kitchen kitchen);
	
	public String prepareLivingRoom(LivingRoom livingRoom);
	
	public String prepareDen(Den den);

}
