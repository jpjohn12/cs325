package frs.alphamon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.GamePoint;
import frs.core.HotGammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.TestObserver;

public class AlphamonTest {
	  private Game game;
	  private HotGammonFactory factory;
	  private Placement[] placementArray;
	  
	  @Before 
	  public void setup() {
		  placementArray = new Placement[] {};
		  factory = new AlphamonFactory();
		  game = new GameImpl(factory);
		  game.addObserver(new TestObserver());
	  }
	  
	  @Test 
	  public void shouldHaveNoPlayerInTurnAfterNewGame() {
	    game.newGame();
	    assertEquals( Color.NONE, game.getPlayerInTurn() );
	  }
	  
	  @Test 
	  public void shouldHaveBlackPlayerInTurnAfterNewGame() {
	    game.newGame();
	    game.nextTurn(); // will throw [1,2] and thus black starts
	    assertEquals( Color.BLACK, game.getPlayerInTurn() );
	  }
	  
	  @Test
	  public void shouldRoll12OnFirstRoll() {
		  game.newGame();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(1, roll[0]);
		  assertEquals(2, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll12OnOn1Mod3() {
		  game.newGame();
		  for (int i = 0; i < 4; i++) {
			  game.nextTurn();
		  }
		  int[] roll = game.diceThrown();
		  assertEquals(1, roll[0]);
		  assertEquals(2, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll34OnSecondRoll() {
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(3, roll[0]);
		  assertEquals(4, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll56OnThirdRoll() {
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(5, roll[0]);
		  assertEquals(6, roll[1]);
	  }
	  
	  @Test
	  public void shouldHave2BlackOnR1() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  assertEquals(2, game.getCount(Location.R1));
	  }
	  
	  @Test
	  public void blackCanMoveR1ToR2AtGameStart() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.move(Location.R1, Location.R2));
		  
		  assertEquals(1, game.getCount(Location.R1));
		  assertEquals(1, game.getCount(Location.R2));
	  }
	  
	  @Test
	  public void blackCannotMoveR1ToB1AtGameStart() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.B1, new GamePoint(Color.RED, 1))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.R1, Location.B1));
	  }
	  
	  @Test
	  public void afterOneBlackMove1MoveLeft() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  game.move(Location.R1, Location.R2);
		  assertEquals(1, game.getNumberOfMovesLeft());
	  }
	  
	  @Test
	  public void afterTwoBlackMovesNoMovesLeft() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.move(Location.R1, Location.R2));
		  assertTrue(game.move(Location.R1, Location.R3));
		  
		  assertEquals(0, game.getNumberOfMovesLeft());
	  }
	  
	  @Test
	  public void redInTurnAfter2ndNextTurn() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  assertEquals(Color.RED, game.getPlayerInTurn());
		  
		  int[] roll = game.diceThrown();
		  assertEquals(3, roll[0]);
		  assertEquals(4, roll[1]);
	  }
	  
	  @Test
	  public void gameShouldEndAfter6Rolls() {
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  assertTrue(game.winner() != Color.NONE);
	  }
	  
	  @Test
	  public void noWinnerUntilGameIsOver() {
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() != Color.NONE);
	  }
	  
	  @Test
	  public void winnerIsAlwaysRed() {
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.NONE);
		  game.nextTurn();
		  assertTrue(game.winner() == Color.RED);
	  }
	  
	  @Test
	  public void canMoveToAnyOpenLocation() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.R2, new GamePoint(Color.RED, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.move(Location.R1, Location.R3));
		  assertFalse(game.move(Location.R1, Location.R2));
		  
	  }
	  
	  @Test
	  public void cannotMoveWhenNotInTurn() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.B1, new GamePoint(Color.RED, 1))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.B1, Location.B2));
	  }

}
