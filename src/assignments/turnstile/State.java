package assignments.turnstile;

public interface State {
	
	public String insertToken();
	
	public String enter();
	
	public String leave();

}
