package frs.variants.startingplayerdeterminer;

import frs.core.StartingPlayerDeterminer;
import frs.hotgammon.Color;

public class DiceRollStartingPlayerDeterminer implements StartingPlayerDeterminer {

	@Override
	public Color determineStartingPlayer(int die1, int die2) {
		if ((die1 - die2) / (Math.abs(die1 - die2)) == -1) {
			return Color.RED;
		}
		else {
			return Color.BLACK;
		}
		
	}

}
