package frs.variants.movevalidator;

import frs.core.MoveValidator;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class SimpleMoveValidator implements MoveValidator {
	private Game game;
	
	public SimpleMoveValidator(Game game) {
		this.game = game;
	}

	@Override
	public boolean isValid(Location from, Location to) {
		if (game.getColor(to) != game.getPlayerInTurn() && game.getCount(to) > 0) {
			return false; 
		}
		return true;
	}

	
}
