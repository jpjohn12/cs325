package assignments.farmer;

public class Operations {
	private String[] operations;
	
	public Operations(String... s) {
		this.operations = s;
	}
	
	public String getPlow() {
		return operations[0];
	}
	
	public void setPlow(String s) {
		operations[0] = s;
	}
	
	public String getPlant() {
		return operations[1];
	}
	public void setPlant(String s) {
		operations[1] = s;
	}
	
	public String getWeedControl() {
		return operations[2];
	}
	public void setWeedControl(String s) {
		operations[2] = s;
	}
	
	public String getHarvest() {
		return operations[3];
	}
	public void setHarvest(String s) {
		operations[3] = s;
	}
	
	public String getMarket() {
		return operations[4];
	}
	public void setMarket(String s) {
		operations[4] = s;
	}

}
