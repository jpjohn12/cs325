package assignments.turnstile;

public class Turnstile {
	private State state;
	
	public Turnstile() {
		this.state = new ClosedState(this);
	}
	
	public String insertToken() {
		return state.insertToken();
	}
	
	public String enter() {
		return state.enter();
	}
	
	public String leave() {
		return state.leave();
	}
	
	public void setState(State state) {
		this.state = state;
	}

}
