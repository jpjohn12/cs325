package assignments.transformation;

public class Split implements Command {
	private Command chain;
	
	@Override
	public void setNext(Command next) {
		this.chain = next;
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("split") && arg != null) {
			if (s.length > 1) {
				System.out.println("Cannot split an array");
				return new String[] {"-1"};
			}
			char[] array = s[0].toCharArray();
			for (int i = 0; i < array.length; i++) {
				if (array[i] == arg.charAt(0)) {
					s = s[0].split(arg);
					return s;
				}
			}

			System.out.println(arg + " does not exist");
			return new String[] {"-1"};
		}
		else {
			return this.chain.execute(command, arg, s);
		}
	}

	@Override
	public Command inverse() {
		return new Join();
	}
	
	public String toString() {
		return "split";
	}

}
