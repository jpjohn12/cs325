package assignments.visitor;

public class Bathroom implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.prepareBathroom(this);
		
	}

}
