package frs.variants.placementprovider;

import frs.core.Board.Placement;
import frs.core.GameImpl.GamePoint;
import frs.core.PlacementProvider;
import frs.hotgammon.Color;
import frs.hotgammon.Location;

public class HyperPlacementProvider implements PlacementProvider {
	private Placement[] placementProvider;

	@Override
	public Placement[] placementArray() {
		placementProvider = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 1)),
											 new Placement(Location.R2, new GamePoint(Color.BLACK, 1)),
											 new Placement(Location.R3, new GamePoint(Color.BLACK, 1)),
											 new Placement(Location.B1, new GamePoint(Color.RED, 1)),
											 new Placement(Location.B2, new GamePoint(Color.RED, 1)),
											 new Placement(Location.B3, new GamePoint(Color.RED, 1))};
		return placementProvider;
	}

}
