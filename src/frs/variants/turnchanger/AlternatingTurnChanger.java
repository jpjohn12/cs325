package frs.variants.turnchanger;

import frs.core.TurnChanger;
import frs.hotgammon.Color;
import frs.hotgammon.Game;

public class AlternatingTurnChanger implements TurnChanger {
	Game game;
	
	public AlternatingTurnChanger(Game game) {
		this.game = game;
	}

	@Override
	public Color changeTurns() {
		if (game.getPlayerInTurn() == Color.RED) {
			return Color.BLACK;
		}
		else if (game.getPlayerInTurn() == Color.BLACK) {
			return Color.RED;
		}
		return Color.NONE;
	}

	
}
