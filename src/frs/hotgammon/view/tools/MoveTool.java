package frs.hotgammon.view.tools;

import java.awt.Point;
import java.awt.event.MouseEvent;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.standard.SelectionTool;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.Convert;
import frs.hotgammon.view.figures.CheckerFigure;

public class MoveTool extends SelectionTool {
	private DrawingEditor editor;
	private Game game;

	private int startX, startY;
	private int currentX, currentY;

	public MoveTool(DrawingEditor editor, Game game) {
		super(editor);
		this.editor = editor;
		this.game = game;
	}

	@Override
	public void mouseDown(MouseEvent e, int x, int y) {
		if (x < 20 || x > 585 || y < 15 || y > 420) {
			return;
		}
		Drawing model = editor().drawing();

		model.lock();
		Point point = new Point(e.getX(), e.getY());
		draggedFigure = model.findFigure(point.x, point.y);
		startX = x;
		startY = y;
		Location location = Convert.xy2Location(startX, startY);
		game.getCount(location);
		if ( draggedFigure != null && draggedFigure instanceof CheckerFigure) {
			if (((CheckerFigure) draggedFigure).getColor() != game.getPlayerInTurn()) {
				editor.showStatus("You can't move a " + ((CheckerFigure) draggedFigure).getColor() + ". It is " + game.getPlayerInTurn() + "'s turn.");
				model.unlock();
				draggedFigure = null;
				return;
			}
			if ( !isTopChecker(point)) {
				draggedFigure = null;
				return;
			}
			fChild = createDragTracker( draggedFigure );
			fChild.mouseDown(e, x, y);
		} else {
			if ( ! e.isShiftDown() ) {
				model.clearSelection();
			}
			fChild = createAreaTracker();
		}

	}

	@Override 
	public void mouseDrag(MouseEvent e, int x, int y) {
		if (draggedFigure != null && draggedFigure instanceof CheckerFigure) {
			fChild.mouseDrag(e, x, y);
			currentX = x;
			currentY = y;
		}
	}

	@Override
	public void mouseUp(MouseEvent e, int x, int y) {
		if (draggedFigure != null && draggedFigure instanceof CheckerFigure) {
			if (x < 20  || x > 585 || y < 15 || y > 420) {
				draggedFigure.moveBy(startX - x, startY - y);
				editor.showStatus("Cannot move a checker off the board. Try again.");
				return;
			}

			Location startingLocation = Convert.xy2Location(startX, startY);
			currentX = draggedFigure.displayBox().x;
			currentY = draggedFigure.displayBox().y;
			Location currentLocation = Convert.xy2Location(currentX, currentY);
			if (isCapture() && distanceEqualsFaceOnDie(startingLocation, currentLocation)) {
				Point point = Convert.locationAndCount2xy(currentLocation, game.getCount(currentLocation) - 1);
				editor.drawing().addToSelection(editor.drawing().findFigure(point.x, point.y));

			}
			if (game.move(startingLocation, currentLocation)) {
				editor.showStatus(game.getPlayerInTurn() + " moved from " + startingLocation + " to " + currentLocation + ". " + 
						game.getPlayerInTurn() + " has " + game.getNumberOfMovesLeft() +" moves left.");
			}
			else {
				editor.showStatus("Move is invalid. Please move again. " + game.getPlayerInTurn() + " has " + game.getNumberOfMovesLeft() +" moves left.");
			}

			if (game.winner() != Color.NONE) {
				editor.showStatus(game.winner() + " Wins!");
				editor.setTool(new DieRollTool(editor, game));
				return;
			}
			
			editor.drawing().unlock();
			fChild = cachedNullTool;
			draggedFigure = null;
			if (game.getNumberOfMovesLeft() == 0) {
				editor.setTool(new DieRollTool(editor, game));
			}
		}
		editor.drawing().selection().clear();
		if (game.getPlayerInTurn() == Color.BLACK && game.getCount(Location.B_BAR) > 0) {
			if (!canMoveOffBar(Location.B_BAR)) {
				editor.showStatus(game.getPlayerInTurn() + " has no valid moves left. Click the dice to end turn.");
				editor.setTool(new DieRollTool(editor, game));
				return;
			}
		}
		else if (game.getPlayerInTurn() == Color.RED && game.getCount(Location.R_BAR) > 0) {
			if (!canMoveOffBar(Location.R_BAR)) {
				editor.showStatus(game.getPlayerInTurn() + " has no valid moves left. Click the dice to end turn.");
				editor.setTool(new DieRollTool(editor, game));
				return;
			}
		}
		boolean hasValidMoves = false;
		for (Location l : Location.values()) {
			if (hasValidMoves(l)) {
				hasValidMoves = true;
			}
		}
		if (hasValidMoves == false) {
			editor.showStatus(game.getPlayerInTurn() + " has no valid moves left. Click the dice to end turn.");
			editor.setTool(new DieRollTool(editor, game));
		}
	}

	private boolean isCapture() {
		Location currentLocation = Convert.xy2Location(currentX, currentY);
		return game.getColor(currentLocation) != game.getPlayerInTurn() && game.getColor(currentLocation) != Color.NONE && game.getCount(currentLocation) == 1;
	}

	private boolean distanceEqualsFaceOnDie(Location from, Location to) {
		for (int die : game.diceThrown()) {
			if (Math.abs(Location.distance(from, to)) == die) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isTopChecker(Point point) {
		Location location = Convert.xy2Location(point.x, point.y);
		if ( location == Location.B1 || location == Location.B2 || location == Location.B3 || location == Location.B4 || location == Location.B5 ||
		         location == Location.B6 || location == Location.B7 || location == Location.B8 || location == Location.B9 || location == Location.B10 ||
		         location == Location.B11 || location == Location.B12 || location == Location.B_BEAR_OFF || location == Location.R_BAR ) {
			if (!(editor.drawing().findFigure(point.x, point.y + 27) instanceof CheckerFigure)) {
				return true;
			}
		}
		else {
			if (!(editor.drawing().findFigure(point.x, point.y - 27) instanceof CheckerFigure)) {
				return true;
			}
		}
		return false;

	}
	
	private boolean canMoveOffBar(Location from) {
		if (hasValidMoves(from)) {
			return true;
		}
		return false;
	}
	
	private boolean hasValidMoves(Location from) {
		if (game.getColor(from) != game.getPlayerInTurn()) {
			return false;
		}
		boolean hasValidMoves = false;
		for (int die : game.diceValuesLeft()) {
			if (game.getColor(Location.findLocation(game.getPlayerInTurn(), from, die)) != game.getPlayerInTurn() &&
					game.getCount(Location.findLocation(game.getPlayerInTurn(), from, die)) > 1) {
				hasValidMoves = false;
			}
			else {
				hasValidMoves = true;
			}
		}
		return hasValidMoves;
	}

}
