package frs.variants.dieroller;

import java.util.Random;

import frs.core.DieRoller;

public class RandomDieRoller implements DieRoller {

	@Override
	public int[] rollDie(int throwCount) {
		int randomDie2 = new Random().nextInt(6) + 1;
		int randomDie1 = new Random().nextInt(6) + 1;
		int[] diceThrows = new int[] {};
		if (randomDie1 == randomDie2) {
			diceThrows = new int[] {randomDie1, randomDie2, randomDie1, randomDie2};
		}
		else {
			diceThrows = new int[] {randomDie1, randomDie2};
		}
		return diceThrows;
	}

	
}
