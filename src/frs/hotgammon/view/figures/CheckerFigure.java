package frs.hotgammon.view.figures;

import java.awt.Point;

import minidraw.standard.ImageFigure;
import frs.hotgammon.Color;

public class CheckerFigure extends ImageFigure {
	private Color color;
	private Point point;
	
	public CheckerFigure(Color color, Point point) {
		super(color.toString().toLowerCase() + "checker", point);
		this.color = color;
		this.point = point;
	}
	
	public Color getColor() {
		return color;
	}
	
	public Point getPoint() {
		return point;
	}

}
