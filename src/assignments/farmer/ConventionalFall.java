package assignments.farmer;

public class ConventionalFall {
	Operations operations;
	
	public ConventionalFall() {
		operations = new Operations("Conventional Plow: no action", "Conventional Plant: no action", "Conventional Weed Control: no action", 
				"Conventional Harvest: hire combine", "Conventional Market: feed corn to elevator");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
