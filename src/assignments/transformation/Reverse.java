package assignments.transformation;

public class Reverse implements Command {
	private Command chain;
	
	@Override
	public void setNext(Command next) {
		this.chain = next;
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("reverse")) {
			String[] s2 = new String[s.length];
			int index = 0;
			for (int i = s.length - 1; i > -1; i--) {
				StringBuilder sb = new StringBuilder();
				sb.append(s[i]);
				s2[index] = sb.reverse().toString();
				index++;
			}
			return s2;
		}
		else {
			return this.chain.execute(command, arg, s);
		}
	}

	@Override
	public Command inverse() {
		return new Reverse();
	}
	
	public String toString() {
		return "reverse";
	}

}
