package frs.core;

import java.util.HashMap;
import java.util.Set;

import frs.core.GameImpl.GamePoint;
import frs.hotgammon.Color;
import frs.hotgammon.Location;

public class Board {
	
	private HashMap<Location, GamePoint> board = new HashMap<Location, GamePoint>();
	public int checkerCount = 0;
	public int blackCheckerCount = 0;
	public int redCheckerCount = 0;
	
	public static class Placement {
		public Location location;
		public GamePoint point;
		public Placement(Location location, GamePoint point) {
			this.location = location;
			this.point = point;
		}
		
	}
	
	public void addPlacement(Placement placement) {
		board.put(placement.location, placement.point);
		checkerCount += placement.point.getCount();
		if (placement.point.getPlayer() == Color.BLACK) {
			blackCheckerCount += placement.point.getCount();
		}
		else if (placement.point.getPlayer() == Color.RED) {
			redCheckerCount += placement.point.getCount();
		}
	}
	
	public void removePlacement(Placement placement) {
		board.remove(placement.location);
		checkerCount -= placement.point.getCount();
	}
	
	public void capture(Color playerInTurn) {
		if (playerInTurn == Color.BLACK) {
			redCheckerCount--;
		}
		else if (playerInTurn == Color.RED) {
			blackCheckerCount--;
		}
	}
	
	public GamePoint get(Location location) {
		return board.get(location);
	}
	
	public Set<Location> keySet() {
		return board.keySet();
	}

	public int getCheckerCount() {
		return checkerCount;
	}

	public int getBlackCheckerCount() {
		return blackCheckerCount;
	}

	public int getRedCheckerCount() {
		return redCheckerCount;
	}

}
