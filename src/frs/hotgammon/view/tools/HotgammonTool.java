package frs.hotgammon.view.tools;

import java.awt.event.MouseEvent;
import java.util.Map;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;
import minidraw.standard.SelectionTool;
import frs.hotgammon.Game;

public class HotgammonTool extends SelectionTool {
	private Tool tool;
	private Game game;
	
	
	private Map<String, Tool> state;

	public HotgammonTool( DrawingEditor editor, Game game, Map<String,Tool> state, String initialState) {
		
		super(editor);
		this.game = game;		
		this.state = state;
		setTool(initialState);
	}

	public void mouseUp(MouseEvent e, int x, int y) { 

	    this.tool.mouseUp(e,x,y);

	}

	public void mouseDrag(MouseEvent e, int x, int y) {

	    this.tool.mouseDrag(e,x,y);
	}

	public void mouseDown(MouseEvent e, int x, int y) {

	    this.tool.mouseDown(e,x,y);
	}

	public void setTool(String toolKey) {
		this.tool = state.get(toolKey);
	}

}
