package frs.hotgammon.view.figures;

import java.awt.Point;

import minidraw.standard.ImageFigure;

public class DieFigure extends ImageFigure {
	int value;
	Point point;
	
	public DieFigure(int value, Point point) {
		super("die" + value, point);
		this.value = value;
		this.point = point;
	}

}
