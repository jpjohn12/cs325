package assignments.visitor;

public class PartyPlanVisitor implements Visitor {

	@Override
	public String prepareBathroom(Bathroom bathroom) {
		return "Bringing clean towels";
		
	}

	@Override
	public String prepareKitchen(Kitchen kitchen) {
		return "Getting the food ready";
		
	}

	@Override
	public String prepareLivingRoom(LivingRoom livingRoom) {
		return "Bringing drinks";
		
	}

	@Override
	public String prepareDen(Den den) {
		return "Bringing chips and nuts";
		
	}

}
