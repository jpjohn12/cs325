package frs.hotgammon.visual;

import javax.swing.JTextField;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.DrawingView;
import minidraw.framework.Factory;
import minidraw.standard.MiniDrawApplication;
import minidraw.standard.StdViewWithBackground;
import frs.betamon.BetamonFactory;
import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.GamePoint;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.Convert;
import frs.hotgammon.view.HotgammonDrawing;
import frs.hotgammon.view.figures.CheckerFigure;
import frs.hotgammon.view.tools.MoveTool;

/** Show the dice and some checkers on the
 * backgammon board.  
 * 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public class ShowCheckersAndDice {
  
  public static void main(String[] args) {
	  BetamonFactory factory = new BetamonFactory();
	  Placement[] placements = new Placement[] {new Placement(Location.R2, new GamePoint(Color.RED, 2)), new Placement(Location.B_BAR, new GamePoint(Color.BLACK, 1))};
	  Game game = new GameImpl(factory);
    DrawingEditor editor = 
      new MiniDrawApplication( "Show HotGammon figures...",  
                               new HotGammonFactory(game) );
    editor.open();

    
    game.addObserver(new HotgammonDrawing(editor, game));
    game.newGame();
    game.nextTurn();
  
    for (Placement p : placements) {
		for (int i = 0; i < p.point.getCount(); i++) {
			editor.drawing().add(new CheckerFigure(p.point.getPlayer(), Convert.locationAndCount2xy(p.location, i)));
		}
	}

    editor.setTool(new MoveTool(editor, game));

  }
}

class HotGammonFactory implements Factory {
	Game game;
	public HotGammonFactory(Game game) {
		this.game = game;
	}
  public DrawingView createDrawingView( DrawingEditor editor ) {
    DrawingView view = 
      new StdViewWithBackground(editor, "board");
    return view;
  }

  public Drawing createDrawing( DrawingEditor editor ) {
    return new HotgammonDrawing(editor, game);
  }

  public JTextField createStatusField( DrawingEditor editor ) {
    JTextField statusField = new JTextField( "Hello HotGammon..." );
    statusField.setEditable(false);
    return statusField;
  }
}


