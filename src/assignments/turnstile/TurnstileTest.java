package assignments.turnstile;

import static org.junit.Assert.*;

import org.junit.*;

public class TurnstileTest {
	Turnstile turnstile;

	@Before
	public void setup() {
		turnstile = new Turnstile();
	}
	
	@Test
	public void cannotEnterUnlessCoinIsInserted() {
		assertEquals(ClosedState.ENTER, turnstile.enter());
	}
	
	@Test
	public void canEnterIfCoinIsInserted() {
		turnstile.insertToken();
		assertEquals(OpenState.ENTER, turnstile.enter());
	}
	
	@Test
	public void willRejectSecondCoin() {
		turnstile.insertToken();
		assertEquals(OpenState.INSERT, turnstile.insertToken());
	}
	
	@Test
	public void turnstileWillStillBeOpenStateWhileLeaving() {
		turnstile.insertToken();
		turnstile.enter();
		assertEquals(OpenState.LEAVE, turnstile.leave());
	}
	
	@Test
	public void turnstileWillChangeToClosedStateAfterLeave() {
		turnstile.insertToken();
		turnstile.enter();
		turnstile.leave();
		assertEquals(ClosedState.ENTER, turnstile.enter());
	}

}
