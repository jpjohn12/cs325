package frs.core;


import frs.hotgammon.Location;

public interface MoveValidator {
	
	/** 
	 * Method for determining if a move is valid or not
	 * @param from
	 * @param to
	 * @return true
	 * @return false
	 */
	public boolean isValid(Location from, Location to);

	
}
