package minidraw.standard;

import java.awt.event.MouseEvent;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.Tool;
import frs.hotgammon.Game;
import frs.hotgammon.view.Convert;

public class HotgammonTool extends SelectionTool implements Tool {
	private Game game;
	
	private int lastX, lastY;
	private int startX, startY;
	
	public HotgammonTool(DrawingEditor editor, Game game) {
		super(editor);
		fChild = cachedNullTool = new NullTool();
	    draggedFigure = null;
	    this.game = game;
	}
	
	@Override
	public void mouseDown(MouseEvent e, int x, int y) {
		startX = x;
		startY = y;
	    Drawing model = editor().drawing();
	    
	    model.lock();

	    draggedFigure = model.findFigure(e.getX(), e.getY());

	    if ( draggedFigure != null ) {
	      fChild = createDragTracker( draggedFigure );
	    } else {
	      if ( ! e.isShiftDown() ) {
	        model.clearSelection();
	      }
	      fChild = createAreaTracker();
	    }
	    fChild.mouseDown(e, x, y);
	    lastX = x;
	    lastY = y;
	 }
	
	@Override
	public void mouseDrag(MouseEvent e, int x, int y) {
		fChild.mouseDrag(e, x, y);
		lastX = x;
		lastY = y;
	}
	
	@Override
	public void mouseUp(MouseEvent e, int x, int y) {
		editor.drawing().unlock();
		fChild.mouseUp(e, x, y);
		if (game.move(Convert.xy2Location(startX, startY), Convert.xy2Location(x, y)) == false) {
			draggedFigure.moveBy(startX - x, startY - y);
		}
		fChild = cachedNullTool;
		draggedFigure = null;
	}

}
