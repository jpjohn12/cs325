package frs.core;

import minidraw.framework.Factory;
import frs.core.Board.Placement;
import frs.hotgammon.Game;

public interface HotGammonFactory extends Factory {
	
	public MoveValidator createMoveValidator();
	
	public WinnerDeterminer createWinnerDeterminer();
	
	public TurnChanger createTurnChanger();
	
	public DieRoller createDieRollCalculator();
	
	public PlacementProvider createPlacementProvider();
	
	public StartingPlayerDeterminer createStartingPlayerDeterminer();
	
	public void setGameImpl(Game game);
	
	public void setPlacement(Placement[] placementArray);
	

}
