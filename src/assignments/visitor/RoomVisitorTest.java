package assignments.visitor;

import static org.junit.Assert.*;

import org.junit.Test;

public class RoomVisitorTest {

	@Test
	public void CleaningBathroomShouldScrubFloor() {
		Room bathroom = new Bathroom();
		Visitor visitor = new CleaningVisitor();
		assertEquals("Scrubbing the floor", bathroom.accept(visitor));
	}
	
	@Test
	public void CleaningKitchenShouldWaxFloor() {
		Room kitchen = new Kitchen();
		Visitor visitor = new CleaningVisitor();
		assertEquals("Waxing the floor", kitchen.accept(visitor));
	}
	
	@Test
	public void CleaningLivingRoomShouldVacuumFloor() {
		Room livingRoom = new LivingRoom();
		Visitor visitor = new CleaningVisitor();
		assertEquals("Vacuuming the floor", livingRoom.accept(visitor));
	}
	
	@Test
	public void CleaningDenShouldSweepFloor() {
		Room den = new Den();
		Visitor visitor = new CleaningVisitor();
		assertEquals("Sweeping the floor", den.accept(visitor));
	}
	
	@Test
	public void PlanningBathroomShouldGetCleanTowels() {
		Room bathroom = new Bathroom();
		Visitor visitor = new PartyPlanVisitor();
		assertEquals("Bringing clean towels", bathroom.accept(visitor));
	}
	
	@Test
	public void PlanningKitchenShouldMakeFood() {
		Room kitchen = new Kitchen();
		Visitor visitor = new PartyPlanVisitor();
		assertEquals("Getting the food ready", kitchen.accept(visitor));
	}
	
	@Test
	public void PlanningLivingRoomShouldGetDrinks() {
		Room livingRoom = new LivingRoom();
		Visitor visitor = new PartyPlanVisitor();
		assertEquals("Bringing drinks", livingRoom.accept(visitor));
	}
	
	@Test
	public void PlanningDenShouldBringChipsAndNuts() {
		Room den = new Den();
		Visitor visitor = new PartyPlanVisitor();
		assertEquals("Bringing chips and nuts", den.accept(visitor));
	}

}
