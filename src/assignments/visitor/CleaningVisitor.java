package assignments.visitor;

public class CleaningVisitor implements Visitor {

	@Override
	public String prepareBathroom(Bathroom bathroom) {
		return"Scrubbing the floor";
		
	}

	@Override
	public String prepareKitchen(Kitchen kitchen) {
		return "Waxing the floor";
		
	}

	@Override
	public String prepareLivingRoom(LivingRoom livingRoom) {
		return "Vacuuming the floor";
		
	}

	@Override
	public String prepareDen(Den den) {
		return "Sweeping the floor";
		
	}

}
