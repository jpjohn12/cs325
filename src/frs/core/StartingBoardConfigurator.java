package frs.core;

import frs.core.Board.Placement;

public interface StartingBoardConfigurator {
	
	public Board initBoard(Placement[] placements);

	
}
