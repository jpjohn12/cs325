package assignments.transformation;

public class Join implements Command {
	private Command chain;
	
	@Override
	public void setNext(Command next) {
		this.chain = next;
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("join") && arg != null) {
			if (s.length == 1) {
				return s;
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < s.length; i++) {
				if (i != s.length - 1) {
					sb.append(s[i] + arg);
				}
				else {
					sb.append(s[i]);
				}
			}
			s = new String[] {sb.toString()};

			return s;
		}
		else {
			return this.chain.execute(command, arg, s);
		}
	}

	@Override
	public Command inverse() {
		return new Split();
	}
	
	public String toString() {
		return "join";
	}

}
