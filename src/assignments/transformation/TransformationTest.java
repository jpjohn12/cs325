package assignments.transformation;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;

public class TransformationTest {
	private Transformer transformer;
	
	private HashMap<String, Command> map = new HashMap<String, Command>() {{
		put("split", new Split());
		put("join", new Join());
		put("uppercase", new Uppercase());
		put("lowercase", new Lowercase());
		put("reverse", new Reverse());
	}};
	
	@Before
	public void setup() {
		transformer = new Transformer(map);
	}

	@Test
	public void uppercaseShouldReturnUppercaseString() {
		transformer.setCommand(new Uppercase());
		transformer.setContext("abc");
		assertEquals("ABC", transformer.execute("uppercase", null).toString());
	}
	
	@Test
	public void uppercaseAndInverseShouldReturnOriginalString() {
		transformer.setCommand(new Uppercase());
		transformer.setContext("abc");
		assertEquals("ABC", transformer.execute("uppercase", null).toString());
		transformer.setCommand(transformer.inverse());
		assertEquals("abc", transformer.execute("lowercase", null).toString());
	}
	
	@Test
	public void lowercaseShouldReturnLowercaseString() {
		transformer.setCommand(new Lowercase());
		transformer.setContext("ABC");
		assertEquals("abc", transformer.execute("lowercase", null).toString());
	}
	
	@Test
	public void lowercaseAndInverseShouldReturnOriginalString() {
		transformer.setCommand(new Lowercase());
		transformer.setContext("ABC");
		assertEquals("abc", transformer.execute("lowercase", null).toString());
		transformer.setCommand(transformer.inverse());
		assertEquals("ABC", transformer.execute("uppercase", null).toString());
	}
	
	@Test
	public void reverseShouldReverseLetterOrder() {
		transformer.setCommand(new Reverse());
		transformer.setContext("abc");
		assertEquals("cba", transformer.execute("reverse", null).toString());
	}
	
	@Test
	public void reverseAndInverseShouldReturnOriginalString() {
		transformer.setCommand(new Reverse());
		transformer.setContext("abc");
		assertEquals("cba", transformer.execute("reverse", null).toString());
		transformer.setCommand(transformer.inverse());
		assertEquals("abc", transformer.execute("reverse", null).toString());
	}
	
	@Test
	public void splitShouldRemoveChosenCharacter() {
		transformer.setCommand(new Split());
		transformer.setContext("abcbc");
		assertEquals("[a, c, c]", transformer.execute("split", "b").toString());
	}
	
	@Test
	public void splitAndInverseShouldReturnOriginalString() {
		transformer.setCommand(new Split());
		transformer.setContext("abcbc");
		assertEquals("[a, c, c]", transformer.execute("split", "b").toString());
		transformer.setCommand(transformer.inverse());
		assertEquals("abcbc", transformer.execute("join", "b").toString());
	}
	
	@Test
	public void joinShouldAddSpecifiedCharacterBetweenEachString() {
		transformer.setCommand(new Join());
		transformer.setContext("abc");
		assertEquals("abc", transformer.execute("join", ":").toString());
	}
	
	@Test
	public void joinAndInverseShouldReturnOriginalString() {
		transformer.setCommand(new Join());
		transformer.setContext("a", "b", "c");
		assertEquals("a:b:c", transformer.execute("join", ":").toString());
		transformer.setCommand(transformer.inverse());
		assertEquals("[a, b, c]", transformer.execute("split", ":").toString());
	}
	
	@Test
	public void undoUppercaseShouldReturnTheLastString() {
		transformer.setCommand(new Uppercase());
		transformer.setContext("abc");
		assertEquals("ABC", transformer.execute("uppercase", null).toString());
		assertEquals("abc", transformer.undo().toString());
	}
	
	@Test
	public void undoLowercaseShouldReturnTheLastString() {
		transformer.setCommand(new Lowercase());
		transformer.setContext("ABC");
		assertEquals("abc", transformer.execute("lowercase", null).toString());
		assertEquals("ABC", transformer.undo());
	}
	
	@Test
	public void undoReverseShouldReturnTheLastString() {
		transformer.setCommand(new Reverse());
		transformer.setContext("abc");
		assertEquals("cba", transformer.execute("reverse", null).toString());
		assertEquals("abc", transformer.undo().toString());
	}
	
	@Test
	public void undoSplitShouldReturnTheLastString() {
		transformer.setCommand(new Split());
		transformer.setContext("abc");
		assertEquals("[a, c]", transformer.execute("split", "b").toString());
		assertEquals("abc", transformer.undo().toString());
	}
	
	@Test
	public void undoJoinShouldReturnTheLastString() {
		transformer.setCommand(new Join());
		transformer.setContext("a", "b", "c");
		assertEquals("a:b:c", transformer.execute("join", ":").toString());
		assertEquals("[a, b, c]", transformer.undo().toString());
	}

}
