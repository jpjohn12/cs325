package frs.hotgammon.view.tools;

import java.awt.event.MouseEvent;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.standard.AbstractTool;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.figures.DieFigure;

public class DieRollTool extends AbstractTool {
	private Game game;
	
	public DieRollTool(DrawingEditor editor, Game game) {
		super(editor);
		this.game = game;
	}
	
	@Override
	public void mouseDown(MouseEvent e, int x, int y) {
	}
	
	@Override
	public void mouseUp(MouseEvent e, int x, int y) {
		if (x < 15 || x > 585 || y < 15 || y > 420) {
			return;
		}
		if (game.winner() != Color.NONE) {
			editor.showStatus(game.winner() + " Wins!");
			return;
		}
		Figure clickedFigure = editor.drawing().findFigure(x, y);
		if (clickedFigure != null && clickedFigure instanceof DieFigure) {
			game.nextTurn();
			if (game.getPlayerInTurn() == Color.BLACK && game.getCount(Location.B_BAR) > 0) {
				if (!canMoveOffBar(Location.B_BAR)) {
					editor.showStatus(game.getPlayerInTurn() + " has no valid moves. Click the dice to end turn.");
					return;
				}
			}
			else if (game.getPlayerInTurn() == Color.RED && game.getCount(Location.R_BAR) > 0) {
				if (!canMoveOffBar(Location.R_BAR)) {
					editor.showStatus(game.getPlayerInTurn() + " has no valid moves. Click the dice to end turn.");
					return;
				}
			}
			boolean hasValidMoves = false;
			for (Location l : Location.values()) {
				if (hasValidMoves(l)) {
					hasValidMoves = true;
				}
			}
			if (hasValidMoves == false) {
				editor.showStatus(game.getPlayerInTurn() + " has no valid moves. Click the dice to end turn.");
				return;
			}
			editor.showStatus("The dice rolled are " + game.diceThrown()[0] + " and " + game.diceThrown()[1] + ". It is " + 
					game.getPlayerInTurn() + "'s turn.");
			editor.setTool(new MoveTool(editor, game));
		}
		
	}
	
	private boolean canMoveOffBar(Location from) {
		if (hasValidMoves(from)) {
			return true;
		}
		return false;
	}
	
	public boolean moveDistanceIsOneFaceOfDie(int distance, int[] diceValuesLeft) {
		for (int die : diceValuesLeft) {
			if (die == distance) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasValidMoves(Location from) {
		if (game.getColor(from) != game.getPlayerInTurn()) {
			return false;
		}
		boolean hasValidMoves = false;
		for (int die : game.diceThrown()) {
			if (game.getColor(Location.findLocation(game.getPlayerInTurn(), from, die)) != game.getPlayerInTurn() &&
					game.getCount(Location.findLocation(game.getPlayerInTurn(), from, die)) > 1) {
				hasValidMoves = false;
			}
			else {
				return true;
			}
		}
		return hasValidMoves;
	}

}
