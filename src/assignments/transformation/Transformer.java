package assignments.transformation;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Transformer {
	private Command command;
	private Context context;
	private String arg;
	private Stack<String[]> stack;
	private Map<String, Command> commands;
	private HashMap<String, Command> inverses = new HashMap<String, Command>() {{
		put("split", new Join());
		put("join", new Split());
		put("uppercase", new Lowercase());
		put("lowercase", new Uppercase());
		put("reverse", new Reverse());
	}};
	
	public Transformer(Map<String, Command> commands) {
		this.commands = commands;
		stack = new Stack<String[]>();
		context = new Context();
	}
	
	public void setCommand(Command command) {
		this.command = command;
	}
	
	public Map<String, Command> getMap() {
		return commands;
	}
	
	public Command inverse() {
		command = inverses.get(command.toString());
		return command;
	}
	
	public Context execute(String command, String arg) {
//		this.command = command;
		this.arg = arg;
		String[] strings = context.getContext();
		String[] newContext = this.command.execute(command, arg, strings);
		if (!newContext[0].equals("-1")) {
			setContext(newContext);
			this.command = commands.get(command);
			String[] array = new String[] {command.toString(), arg};
			stack.push(array);
		}
		return context;
		
//		
//		stack.push(context);
//		context = stack.peek();
//		return context;
	}
	
	public String undo() {
		if (stack.size() > 0) {
			String[] s = stack.pop();
			command = commands.get(s[0]).inverse();
			arg = s[1];
			execute(command.toString(), arg);
			stack.pop();
			return context.toString();
		}
		else {
			System.out.println("There is nothing to undo");
			return context.toString();
		}
	}

	public void setContext(String... context) {
		this.context.setContext(context);
	}
	
	public String[] getContext() {
		return context.getContext();
	}

}
