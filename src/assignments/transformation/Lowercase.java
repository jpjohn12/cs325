package assignments.transformation;

public class Lowercase implements Command {
	private static final Command inverse = new Uppercase();
	private Command chain;

	@Override
	public void setNext(Command next) {
		this.chain = next;
	}

	@Override
	public String[] execute(String command, String arg, String... s) {
		if (command.equals("lowercase")) {
			for (int i = 0; i < s.length; i++) {
				s[i] = s[i].toLowerCase();
			}
			return s;
		} 
		else {
			return this.chain.execute(command, arg, s);
		}

	}

	@Override
	public Command inverse() {
		return inverse;
	}

	public String toString() {
		return "lowercase";
	}

}
