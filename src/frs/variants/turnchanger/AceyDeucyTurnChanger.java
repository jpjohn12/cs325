package frs.variants.turnchanger;

import frs.core.TurnChanger;
import frs.hotgammon.Color;
import frs.hotgammon.Game;

public class AceyDeucyTurnChanger implements TurnChanger {
	Game game;
	
	public AceyDeucyTurnChanger(Game game) {
		this.game = game;
	}

	@Override
	public Color changeTurns() {
		if ((game.diceThrown()[0] == 2 && game.diceThrown()[1] == 1) ||
				(game.diceThrown()[0] == 1 && game.diceThrown()[1] == 2)) {
			return game.getPlayerInTurn();
		}
		else {
			if (game.getPlayerInTurn() == Color.RED) {
				return Color.BLACK;
			}
			else if (game.getPlayerInTurn() == Color.BLACK) {
				return Color.RED;
			}
			return Color.NONE;
		}
	}

	
}
