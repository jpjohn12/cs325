package frs.variants.startingplayerdeterminer;

import frs.core.StartingPlayerDeterminer;
import frs.hotgammon.Color;

public class BlackStartingPlayerDeterminer implements StartingPlayerDeterminer {

	@Override
	public Color determineStartingPlayer(int die1, int die2) {
		return Color.BLACK;
		
	}

}
