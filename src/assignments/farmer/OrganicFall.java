package assignments.farmer;

public class OrganicFall {
	Operations operations;
	
	public OrganicFall() {
		operations = new Operations("Organic Plow: no action", "Organic Plant: late beans, squash, potatoes, leafy greens", "Organic Weed Control: no action",
				"Organic Harvest: employ lots of interns and volunteers; u-pick for squash", "Organic Market: beans, squash, tomatoes to farmer's market; big harvest party on farm");
		
	}
	
	public Operations getOperations() {
		return operations;
	}

}
