package assignments.visitor;

public class LivingRoom implements Room {

	@Override
	public String accept(Visitor visitor) {
		return visitor.prepareLivingRoom(this);
		
	}

}
