package frs.semimon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.GamePoint;
import frs.core.HotGammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.TestObserver;

public class SemimonTest {
	private HotGammonFactory factory;
	private Game game;
	private Placement[] placementArray;

	@Before
	public void setup() {
		placementArray = new Placement[] {};
		factory = new SemimonFactory();
		game = new GameImpl(factory);
		game.addObserver(new TestObserver());
	}
	
	@Test
	  public void checkerCanOnlyMoveInSpecificDirection() {
		  placementArray = new Placement[] {new Placement(Location.R6, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.R6, Location.R5));
	  }
	  
	  @Test
	  public void distanceMovedMustEqualValueOfARolledDie() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  int[] dice = game.diceThrown();
		  assertTrue(game.move(Location.R1, Location.findLocation(Color.BLACK, Location.R1, dice[0])));
		  assertFalse(game.move(Location.R1, Location.R8));
	  }
	  
	  @Test
	  public void cannotMovetoBlockedPoint() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.R3, new GamePoint(Color.RED, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.R1, Location.R3));
	  }
	  
	  @Test
	  public void redCheckerInBarMustMoveToBlacksInnerTable() {
		  placementArray = new Placement[] {new Placement(Location.R_BAR, new GamePoint(Color.RED, 1)), new Placement(Location.R8, new GamePoint(Color.BLACK, 1))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(Color.RED, game.getPlayerInTurn());
		  assertTrue(game.move(Location.R_BAR, Location.findLocation(Color.RED, Location.R_BAR, roll[0])));
	  }
	  
	  @Test
		public void dieShouldBeGreaterThanZero() {
			game.newGame();
			game.nextTurn();
			int[] diceRolled = game.diceThrown();
			assertTrue(diceRolled[0] > 0);
			assertTrue(diceRolled[1] > 0);
			game.nextTurn();
			diceRolled = game.diceThrown();
			assertTrue(diceRolled[0] > 0);
			assertTrue(diceRolled[1] > 0);
		}
		
		@Test
		public void dieShouldBeLessThanSeven() {
			game.newGame();
			game.nextTurn();
			int[] diceRolled = game.diceThrown();
			assertTrue(diceRolled[0] < 7);
			assertTrue(diceRolled[1] < 7);
			game.nextTurn();
			diceRolled = game.diceThrown();
			assertTrue(diceRolled[0] < 7);
			assertTrue(diceRolled[1] < 7);
		}
		
		@Test
		public void winnerShouldReturnBlackIfAllBlacksCheckersAreBorneOff() {
			placementArray = new Placement[] {new Placement(Location.B1, new GamePoint(Color.BLACK, 1)), new Placement(Location.R1, new GamePoint(Color.RED, 1))};
			factory.setPlacement(placementArray);
			game.newGame();
			game.nextTurn();
			assertEquals(Color.NONE, game.winner());
			assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
			assertEquals(Color.BLACK, game.winner());
		}
		
		@Test
		public void winnerShouldReturnRedIfAllRedsCheckersAreBorneOff() {
			placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 1)), new Placement(Location.R1, new GamePoint(Color.RED, 1))};
			factory.setPlacement(placementArray);
			game.newGame();
			game.nextTurn();
			assertEquals(Color.NONE, game.winner());
			game.nextTurn();
			assertTrue(game.move(Location.R1, Location.R_BEAR_OFF));
			assertEquals(Color.RED, game.winner());
		}

		
}
