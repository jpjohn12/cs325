package assignments.transformation;

public class Context {
	private String[] context;
	
	public Context() {
		
	}
	
	public Context(String... context) {
		this.context = context;
	}
	
	public String[] getContext() {
		return context;
	}
	
	public void setContext(String... context) {
		this.context = context;
	}
	
	@Override
	public String toString() {
		if (context.length > 1) {
			StringBuilder sb = new StringBuilder();
			sb.append("[");
			for (int i = 0; i < context.length - 1; i++) {
				if (!context[i].equals(null)) {
					sb.append(context[i] + ", ");
				}
			}
			sb.append(context[context.length - 1] + "]");
			return sb.toString();
		}
		else {
			return context[0];
		}
	}

}
