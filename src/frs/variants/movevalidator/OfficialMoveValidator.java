package frs.variants.movevalidator;

import frs.core.GameImpl;
import frs.core.MoveValidator;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;

public class OfficialMoveValidator implements MoveValidator {
	private Game game;
	
	public OfficialMoveValidator(Game game) {
		this.game = game;
	}
	
	@Override
	public boolean isValid(Location from, Location to) {
        if (to == Location.B_BEAR_OFF || to == Location.R_BEAR_OFF) {
        	if ((to == Location.B_BEAR_OFF && game.getPlayerInTurn() == Color.RED) || (to == Location.R_BEAR_OFF && game.getPlayerInTurn() == Color.BLACK)) {
        		return false;
        	}
    		for (int die : game.diceValuesLeft()) {
	        	if (!canBearOff()) {
	        		return false;
	        	}
	        	else if (Math.abs(Location.distance(from, Location.R_BEAR_OFF)) == die || Math.abs(Location.distance(from, Location.B_BEAR_OFF)) == die) {
		            return true;
	        	}
	            else if (canBearOffLowerChecker(from)) {
		            return true;
	    		}
	            
    		}
        	
        	return false;
        }
        if (checkerOnBar()) {
        	if ((from != Location.B_BAR && game.getPlayerInTurn() == Color.BLACK) || (from != Location.R_BAR && game.getPlayerInTurn() == Color.RED)) {
        		return false;
        	}
        }
        if (moveIsInCorrectDirection(Location.distance(from, to))
        		&& moveDistanceIsOneFaceOfDie(Math.abs(Location.distance(from, to)))
        		&& isValidPlayerOrOneOpponent(from, to)) {
//        	if (isCapturing(to)) {
//        		
//        		if (game.getPlayerInTurn() == Color.BLACK) {
//        			board.addPlacement(new Placement(Location.R_BAR, new GamePoint(Color.RED, board.get(Location.R_BAR).getCount() + 1)));
//        		}
//        		else {
//        			board.addPlacement(new Placement(Location.B_BAR, new GamePoint(Color.BLACK, board.get(Location.B_BAR).getCount() + 1)));
//        		}
//                board.capture(playerInTurn);
//        	}
//        	else {
//	            toPoint.setCount(toPoint.getCount() + 1);
//	       	}
//        	toPoint.setPlayer(playerInTurn);
//            fromPoint.setCount(fromPoint.getCount() - 1);
//        	if (fromPoint.getCount() == 0) {
//    			fromPoint.setPlayer(Color.NONE);
//    		}
            return true;
          
        }
        return false;
        
    }
	
	private boolean canBearOff() {
		
		if (game.getPlayerInTurn() == Color.BLACK) {
			int count = game.getCount(Location.B1) + game.getCount(Location.B2) + game.getCount(Location.B3) + 
					game.getCount(Location.B4) + game.getCount(Location.B5) + game.getCount(Location.B6) +
					game.getCount(Location.B_BEAR_OFF);
			Location[] home = {Location.B1, Location.B2, Location.B3, Location.B4, Location.B5, Location.B6, Location.B_BEAR_OFF};
			for (Location l : home) {
				if (game.getColor(l) == Color.RED) {
					count -= game.getCount(l);
				}
			}
			if (count == ((GameImpl) game).getBlackCheckerCount()) {
				return true;
			}
		}
		if (game.getPlayerInTurn() == Color.RED) {
			int count = game.getCount(Location.R1) + game.getCount(Location.R2) + game.getCount(Location.R3) + 
					game.getCount(Location.R4) + game.getCount(Location.R5) + game.getCount(Location.R6) +
					game.getCount(Location.R_BEAR_OFF);
			Location[] home = {Location.R1, Location.R2, Location.R3, Location.R4, Location.R5, Location.R6, Location.R_BEAR_OFF};
			for (Location l : home) {
				if (game.getColor(l) == Color.BLACK) {
					count -= game.getCount(l);
				}
			}
			if (count == ((GameImpl) game).getRedCheckerCount()) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean canBearOffLowerChecker(Location from) {
		boolean canBearOff = false;
		Location[] locations;
		if (game.getPlayerInTurn() == Color.BLACK) {
			locations = new Location[] {Location.B1, Location.B2, Location.B3, Location.B4, Location.B5, Location.B6};
		}
		else {
			locations = new Location[] {Location.R1, Location.R2, Location.R3, Location.R4, Location.R5, Location.R6};
		}
		for (Location location : locations) {
			for (int die : game.diceValuesLeft()) {
				if (Math.abs(Location.distance(from, Location.B_BEAR_OFF)) < die || Math.abs(Location.distance(from, Location.R_BEAR_OFF)) < die) {
					int locNum = (int)location.toString().charAt(1);
					int fromNum = (int)from.toString().charAt(1);
					if (locNum > fromNum) {
						if (game.getColor(location) != game.getPlayerInTurn()) {
							canBearOff = true;
						}
						else {
							return false;
						}
					}
				}
				
			}
		}
		
		return canBearOff;
	}

	public boolean checkerOnBar() {
		return (game.getPlayerInTurn() == Color.BLACK && game.getCount(Location.B_BAR) > 0) ||
				(game.getPlayerInTurn() == Color.RED && game.getCount(Location.R_BAR) > 0);
		
	}
	
	public boolean isCheckerOnOpponentBar() {
		if (game.getPlayerInTurn() == Color.BLACK) {
			return game.getCount(Location.R_BAR) > 0;
		}
		else {
			return game.getCount(Location.B_BAR) > 0;
		}
	}
	
	public boolean isValidPlayerOrOneOpponent(Location from, Location to) {
		return game.getColor(to) == game.getColor(from) || game.getCount(to) == 1
				|| game.getColor(to) == Color.NONE;
	}
	
//	public boolean isCapturing(Location to) {
//		return game.getPlayerInTurn() != game.getColor(to) && game.getCount(to) == 1;
//	}
	
	public boolean moveIsInCorrectDirection(int distance) {
		return distance / game.getPlayerInTurn().getSign() > 0;
	}
	
	public boolean moveDistanceIsOneFaceOfDie(int distance) {
		for (int die : game.diceValuesLeft()) {
			if (die == distance) {
				return true;
			}
		}
		return false;
	}

}
