package frs.core;

import frs.hotgammon.Color;


public interface TurnChanger {
	
	public Color changeTurns();

}
