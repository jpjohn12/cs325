package frs.gammamon;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import frs.core.Board.Placement;
import frs.core.GameImpl;
import frs.core.GameImpl.GamePoint;
import frs.core.HotGammonFactory;
import frs.hotgammon.Color;
import frs.hotgammon.Game;
import frs.hotgammon.Location;
import frs.hotgammon.view.TestObserver;

public class GammamonTest {
	private HotGammonFactory factory;
	private Game game;
	private Placement[] placementArray;
	
	@Before
	public void setup() {
		factory = new GammamonFactory();
		game = new GameImpl(factory);
		game.addObserver(new TestObserver());
	}
	
	@Test 
	  public void shouldHaveNoPlayerInTurnAfterNewGame() {
	    game.newGame();
	    assertEquals( Color.NONE, game.getPlayerInTurn() );
	  }
	  
	  @Test 
	  public void shouldHaveBlackPlayerInTurnAfterNewGame() {
	    game.newGame();
	    game.nextTurn(); // will throw [1,2] and thus black starts
	    assertEquals( Color.BLACK, game.getPlayerInTurn() );
	  }
	  
	  @Test
	  public void shouldRoll12OnFirstRoll() {
		  game.newGame();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(1, roll[0]);
		  assertEquals(2, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll12OnOn1Mod3() {
		  game.newGame();
		  for (int i = 0; i < 10; i++) {
			  game.nextTurn();
		  }
		  int[] roll = game.diceThrown();
		  assertEquals(1, roll[0]);
		  assertEquals(2, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll34OnSecondRoll() {
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(3, roll[0]);
		  assertEquals(4, roll[1]);
	  }
	  
	  @Test
	  public void shouldRoll56OnThirdRoll() {
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(5, roll[0]);
		  assertEquals(6, roll[1]);
	  }
	  
	  @Test
	  public void shouldHave2BlackOnR1() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  assertEquals(2, game.getCount(Location.R1));
	  }
	  
	  @Test
	  public void blackCanMoveR1ToR2AtGameStart() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.move(Location.R1, Location.R2));
		  
		  assertEquals(1, game.getCount(Location.R1));
		  assertEquals(1, game.getCount(Location.R2));
	  }
	  
	  @Test
	  public void blackCannotMoveR1ToB1AtGameStart() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.B1, new GamePoint(Color.RED, 1))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.R1, Location.B1));
	  }
	  
	  @Test
	  public void afterOneBlackMove1MoveLeft() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  game.move(Location.R1, Location.R2);
		  assertEquals(1, game.getNumberOfMovesLeft());
	  }
	  
	  @Test
	  public void afterTwoBlackMovesNoMovesLeft() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  int[] roll = game.diceThrown();
		  assertEquals(1, roll[0]);
		  assertEquals(2, roll[1]);
		  assertTrue(game.move(Location.R1, Location.R2));
		  assertTrue(game.move(Location.R1, Location.R3));
		  
		  assertEquals(0, game.getNumberOfMovesLeft());
	  }
	  
	  @Test
	  public void redInTurnAfter2ndNextTurn() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  game.nextTurn();
		  assertEquals(Color.RED, game.getPlayerInTurn());
		  
		  int[] roll = game.diceThrown();
		  assertEquals(3, roll[0]);
		  assertEquals(4, roll[1]);
	  }
	  
	  @Test
	  public void canMoveToAnyOpenLocation() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.R2, new GamePoint(Color.RED, 2))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertTrue(game.move(Location.R1, Location.R3));
		  assertFalse(game.move(Location.R1, Location.R2));
		  
	  }
	  
	  @Test
	  public void cannotMoveWhenNotInTurn() {
		  placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 2)), new Placement(Location.B1, new GamePoint(Color.RED, 1))};
		  factory.setPlacement(placementArray);
		  game.newGame();
		  game.nextTurn();
		  assertFalse(game.move(Location.B1, Location.B2));
	  }
	
	@Test
	public void cannotBeWinnerIfNoCheckersAreOnBoard() {
		game.newGame();
		game.nextTurn();
		assertEquals(Color.NONE, game.winner());
	}

	@Test
	public void shouldReturnNoneIfAPlayersCheckersAreStillOnBoard() {
		placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 1)), new Placement(Location.B1, new GamePoint(Color.RED, 1))};
		factory.setPlacement(placementArray);
		game.newGame();
		game.nextTurn();
		assertEquals(Color.NONE, game.winner());
	}
	
	@Test
	public void gameShouldLastLongerThanSixTurns() {
		game.newGame();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		game.nextTurn();
		assertEquals(Color.NONE, game.winner());
	}
	
	@Test
	public void winnerShouldReturnBlackIfAllBlacksCheckersAreBorneOff() {
		placementArray = new Placement[] {new Placement(Location.B1, new GamePoint(Color.BLACK, 1)), new Placement(Location.R4, new GamePoint(Color.RED, 1))};
		factory.setPlacement(placementArray);
		game.newGame();
		game.nextTurn();
		assertEquals(Color.NONE, game.winner());
		assertTrue(game.move(Location.B1, Location.B_BEAR_OFF));
		assertEquals(Color.BLACK, game.winner());
	}
	
	@Test
	public void winnerShouldReturnRedIfAllRedsCheckersAreBorneOff() {
		placementArray = new Placement[] {new Placement(Location.R1, new GamePoint(Color.BLACK, 1)), new Placement(Location.R4, new GamePoint(Color.RED, 1))};
		factory.setPlacement(placementArray);
		game.newGame();
		game.nextTurn();
		assertEquals(Color.NONE, game.winner());
		game.nextTurn();
		assertTrue(game.move(Location.R4, Location.R_BEAR_OFF));
		assertEquals(Color.RED, game.winner());
	}

	
}
