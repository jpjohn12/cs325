package frs.core;

import frs.hotgammon.Color;

public interface StartingPlayerDeterminer {
	
	public Color determineStartingPlayer(int die1, int die2);

}
